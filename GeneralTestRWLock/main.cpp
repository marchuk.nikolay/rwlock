#include <iostream>
#include <mutex>

#include "RWLock.h"
#include "ReadGuard.h"
#include "WriteGuard.h"

class ThreadSafeCounter
{
public:
    unsigned int Get()
    {
        ReadGuard guard(rwLock_);
        return value_;
    }

    void Increment()
    {
        WriteGuard guard(rwLock_);
        ++value_;
    }

    void Reset()
    {
        WriteGuard guard(rwLock_);
        value_ = 0;
    }

    void PrintValue()
    {
        ReadGuard guard(rwLock_);
        std::cout << std::this_thread::get_id() << ' ' << value_ << '\n';
    }

private:
    RWLock rwLock_;
    unsigned int value_ = 0;
};

int main()
{
    ThreadSafeCounter counter;

    auto IncrementAndPrint = [&counter]()
    {
        for (int i = 0; i < 3; ++i)
        {
            counter.Increment();
            counter.PrintValue();
        }
    };

    std::thread thread1(IncrementAndPrint);
    std::thread thread2(IncrementAndPrint);

    thread1.join();
    thread2.join();

    return 0;
}
#pragma once

class RWLock;
class ReadGuard;
class WriteGuard;

class UpgradeGuard
{
public:
    UpgradeGuard(RWLock& rwlock, ReadGuard& readGuard);
    ~UpgradeGuard();

    UpgradeGuard(const UpgradeGuard& rhs) = delete;
    UpgradeGuard& operator=(const UpgradeGuard& rhs) = delete;

private:
    RWLock* rwLock_;
    ReadGuard* readGuard_;
    std::unique_ptr<WriteGuard> writeGuard_ = nullptr;
};
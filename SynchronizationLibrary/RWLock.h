#pragma once

class RWLock
{
public:
    RWLock();

    RWLock(const RWLock& rhs) = delete;
    RWLock& operator=(const RWLock& rhs) = delete;

    void LockRead();
    void UnlockRead();

    void LockWrite();
    void UnlockWrite();

private:
    bool writing_;
    unsigned int readersCount_;

    std::mutex mutex_;
    std::condition_variable readQueue_;
    std::condition_variable writeQueue_;
};
#pragma once

class RWLock;

class ReadGuard
{
public:
    explicit ReadGuard(RWLock& rwlock);
    ~ReadGuard();

    ReadGuard(const ReadGuard& rhs) = delete;
    ReadGuard& operator=(const ReadGuard& rhs) = delete;

    void Lock();
    void Unlock();

private:
    RWLock* rwlock_;
    bool owns_;
};

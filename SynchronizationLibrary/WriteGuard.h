#pragma once

class RWLock;

class WriteGuard
{
public:
    explicit WriteGuard(RWLock& rwlock);
    ~WriteGuard();

    WriteGuard(const WriteGuard& rhs) = delete;
    WriteGuard& operator=(const WriteGuard& rhs) = delete;

    void Lock();
    void Unlock();

private:
    RWLock* rwlock_;
    bool owns_;
};

#include "stdafx.h"
#include "UpgradeGuard.h"
#include "ReadGuard.h"
#include "WriteGuard.h"

UpgradeGuard::UpgradeGuard(RWLock& rwlock, ReadGuard& readGuard)
    : rwLock_(&rwlock)
    , readGuard_(&readGuard)
{
    readGuard_->Unlock();

    writeGuard_.reset(new WriteGuard(*rwLock_));
}

UpgradeGuard::~UpgradeGuard()
{
    writeGuard_->Unlock();
}
#include "stdafx.h"
#include "RWLock.h"

RWLock::RWLock()
    : readersCount_(0)
    , writing_(false)
    , mutex_()
    , readQueue_()
    , writeQueue_()
{
}

void RWLock::LockRead()
{
    std::unique_lock<std::mutex> lock(mutex_);

    writeQueue_.wait(lock, [&]() {
        return !writing_;
    });

    ++readersCount_;
}

void RWLock::UnlockRead()
{
    std::lock_guard<std::mutex> lock(mutex_);

    --readersCount_;

    if (writing_ && !readersCount_)
    {
        readQueue_.notify_one();
    }
    else if (!writing_)
    {
        writeQueue_.notify_all();
    }
}

void RWLock::LockWrite()
{
    std::unique_lock<std::mutex> lock(mutex_);

    writeQueue_.wait(lock, [&]() {
        return !writing_;
    });

    writing_ = true;

    readQueue_.wait(lock, [&]() {
        return !readersCount_;
    });
}

void RWLock::UnlockWrite()
{
    std::lock_guard<std::mutex> lock(mutex_);

    writing_ = false;

    writeQueue_.notify_all();
}
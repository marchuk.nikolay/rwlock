#include "stdafx.h"
#include "WriteGuard.h"
#include "RWLock.h"

WriteGuard::WriteGuard(RWLock& rwlock)
    : rwlock_(&rwlock)
    , owns_(true)
{
    rwlock_->LockWrite();
}

WriteGuard::~WriteGuard()
{
    if (owns_)
    {
        rwlock_->UnlockWrite();
    }
}

void WriteGuard::Lock()
{
    if (!rwlock_)
    {
        throw std::runtime_error("RWLock == nullptr");
    }

    rwlock_->LockWrite();
    owns_ = true;
}

void WriteGuard::Unlock()
{
    if (!rwlock_)
    {
        throw std::runtime_error("RWLock == nullptr");
    }

    if (!owns_)
    {
        throw std::runtime_error("RWLock has already unlocked (write)");
    }

    rwlock_->UnlockWrite();
    owns_ = false;
}
#include "stdafx.h"
#include "ReadGuard.h"
#include "RWLock.h"

ReadGuard::ReadGuard(RWLock& rwlock)
    : rwlock_(&rwlock)
    , owns_(true)
{
    rwlock_->LockRead();
}

ReadGuard::~ReadGuard()
{
    if (owns_)
    {
        rwlock_->UnlockRead();
    }
}

void ReadGuard::Lock()
{
    if (!rwlock_)
    {
        throw std::runtime_error("RWLock == nullptr");
    }

    rwlock_->LockRead();
    owns_ = true;
}

void ReadGuard::Unlock()
{
    if (!rwlock_)
    {
        throw std::runtime_error("RWLock == nullptr");
    }

    if (!owns_)
    {
        throw std::runtime_error("RWLock has already unlocked (read)");
    }

    rwlock_->UnlockRead();
    owns_ = false;
}
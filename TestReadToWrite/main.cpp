#include <vector>
#include <iostream>
#include <mutex>

#include "RWLock.h"
#include "ReadGuard.h"
#include "WriteGuard.h"
#include "UpgradeGuard.h"

std::vector<int> data;
RWLock rwLock;

void Write(int val)
{
    WriteGuard wg(rwLock);

    std::cout << "Write value " << val << "\n";

    data.push_back(val);
}

void Read()
{
    ReadGuard rg(rwLock);

    if (!data.empty())
    {
        std::cout << "Read value " << data.back() << "\n";
    }
}

void ReadWrite(int val)
{
    ReadGuard rg(rwLock);

    if (!data.empty())
    {
        std::cout << "ReadWrite Read value " << data.back() << "\n";
    }

    UpgradeGuard(rwLock, rg);

    data.push_back(val);

    std::cout << "ReadWrite Write value " << data.back() << "\n";
}

int main()
{
    std::vector<std::thread> threads;

    for (int i = 0; i < 10; ++i)
    {
        if (i == 5)
        {
            threads.emplace_back(ReadWrite, i);
            continue;
        }

        if (i % 2 == 0)
        {
            threads.emplace_back(Write, i);
        }
        else
        {
            threads.emplace_back(Read);
        }
    }

    for (auto& thread : threads)
    {
        thread.detach();
    }

    return 0;
}
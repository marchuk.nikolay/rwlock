#include <iostream>
#include <mutex>

#include "RWLock.h"
#include "ReadGuard.h"

int i = 5;
RWLock rwLock;

void test()
{
    ReadGuard guard(rwLock);
    std::cout << i << "\n";
}

int main()
{
    ReadGuard guard(rwLock);
    std::cout << i << "\n";

    test();

    return 0;
}